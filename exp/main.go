package main

import (
	"bufio"
	"fmt"
	"github.com/jinzhu/gorm"
	"os"
	"strings"

	"btphoto.com/models"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

const (
	host     = "127.0.0.1"
	port     = 5432
	user     = "postgres"
	password = "postgres"
	dbname   = "postgres"
)

func main() {
	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=disable",
		host, port, user, password, dbname)
	db, err := gorm.Open("postgres", psqlInfo)
	if err != nil {
		panic(err)
	}
	defer db.Close()
	db.LogMode(true)
	db.AutoMigrate(&models.User{}, &models.Order{})

	name, email := getInfo()
	u := &models.User{
		Name:  name,
		Email: email,
	}
	if err = db.Create(u).Error; err != nil {
		panic(err)
	}
	fmt.Println(u)

	var user models.User
	db.First(&user)
	if db.Error != nil {
		panic(db.Error)
	}

	var findUser2 models.User
	maxId := 3
	db.Where("id <= ?", maxId).First(&findUser2)

	createOrder(db, user, 1001, "Fake Description #1")
	createOrder(db, user, 9999, "Fake Description #2")
	createOrder(db, user, 8800, "Fake Description #3")
}

func createOrder(db *gorm.DB, user models.User, amount int, desc string) {
	db.Create(&models.Order{
		UserID:      user.ID,
		Amount:      amount,
		Description: desc,
	})
	if db.Error != nil {
		panic(db.Error)
	}
}

func getInfo() (name, email string) {
	reader := bufio.NewReader(os.Stdin)
	fmt.Println("What is your name?")
	name, _ = reader.ReadString('\n')
	name = strings.TrimSpace(name)
	fmt.Println("What is your email?")
	email, _ = reader.ReadString('\n')
	email = strings.TrimSpace(email)
	return name, email
}
