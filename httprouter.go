package main

import (
	"fmt"
	"github.com/julienschmidt/httprouter"
	"net/http"
)

func httpHome(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	w.Header().Set("Content-Type", "text/html")
	fmt.Fprint(w, "<h1>Hello, World!</h1>")
}

func httpContact(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	w.Header().Set("Content-Type", "text/html")
	fmt.Fprint(w, "To get in touch, please send an email "+
		"to <a href=\"mailto:support@lenslocked.com\">"+
		"support@lenslocked.com</a>.")
}

func httpFaq(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	w.Header().Set("Content-Type", "text/html")
	fmt.Fprint(w, "This is the beginning of an FAQ")
}

func httpNotFound(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusNotFound)
	fmt.Fprint(w, "<h1>We could not find the page you "+
		"were looking for :(</h1>"+
		"<p>Please email us if you keep being sent to an "+
		"invalid page.</p>")
}

func main() {
	r := httprouter.New()
	r.GET("/", httpHome)
	r.GET("/contact", httpContact)
	r.GET("/faq", httpFaq)
	nf := http.HandlerFunc(httpNotFound)
	r.NotFound = nf
	http.ListenAndServe(":3000", r)
}
